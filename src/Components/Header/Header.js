import './Header.css';
import User from '../User/User';
import logo from '../../assets/img/logo.svg';

function Header() {
  return (
    <div className="navbar">
      <div className="logo">
        <img src={logo} className="App-logo" alt="logo" width="150" />
      </div>
      <User />
    </div>
  );
}

export default Header;