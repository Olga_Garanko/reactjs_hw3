import './App.css';
import Header from '../Components/Header/Header';
import Menu from '../Components/Menu/Menu';
import Main from '../Components/Main/Main';

function App() {
  return (
    <div className="App">
      <div className="wrapper">
        <Header />
        <section className="section">
          <Menu />
          <Main />       
        </section>
      </div>
    </div>
  );
}

export default App;
